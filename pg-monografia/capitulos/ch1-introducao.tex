% ==============================================================================
% TCC - Mateus Tassinari Ferreira
% Capítulo 1 - Introdução
% ==============================================================================
\chapter{Introdução}
\label{sec-intro}

No início da \textit{World Wide Web}, a infraestrutura de software por trás dos servidores dava apoio somente a páginas estáticas, ou seja, documentos entregues ao cliente como retorno a requisições HTTP. A partir de 1993, com o surgimento da tecnologia CGI e de linguagens de programação para a \textit{Web}, os servidores ficaram mais poderosos, admitindo o desenvolvimento de aplicações de negócio, em que grandes sistemas começaram a ser desenvolvidos para a \textit{Web}.

Nesse momento surge o conceito de \textit{WebApp}, que consiste em um conjunto de páginas \textit{Web} que interagem com o usuário, fornecendo e processando informações. Exemplos são websites informativos, interativos, aplicações transacionais ou baseadas em fluxos que executam na \textit{Web}, ambientes colaborativos de trabalho, comunidades online e portais~\cite{ginige2001web}. Nesta monografia, no entanto, iremos focar em uma categoria de aplicações \textit{Web}, a qual chamamos Sistemas de Informação Baseados na Web (\textit{Web-based Information Systems} - WISs), que são como sistemas de informação tradicionais, porém disponíveis na \textit{Internet}, como ambientes cooperativos, sistemas de gerencia empresarial, dentre muitos outros.

As primeiras aplicações \textit{WebApps} eram, geralmente, desenvolvidas de maneira \textit{ad hoc}, sem considerar princípios de Engenharia de Software. Entretanto, com a demanda por maior rapidez e qualidade dessas aplicações, surge uma necessidade de abordagens disciplinadas de Engenharia de Software. Tais abordagens e técnicas devem levar em conta as características do ambiente e os cenários operacionais e a vasta variedade de perfis de usuários, que adicionam desafios ao desenvolvimento de aplicações \textit{Web}. Nasceria, assim, a Engenharia Web~\cite{pressman2011engenharia}.

A Engenharia Web pode ser definida como o estabelecimento e uso de princípios científicos, de engenharia e de gerência, além de abordagens sistemáticas, para o bem-sucedido desenvolvimento, implantação e manutenção de aplicações e sistemas \textit{Web} de alta qualidade~\cite{murugesan1999web}.

Neste novo ramo, muitos métodos, \textit{frameworks} e linguagens de programação para desenvolvimento de aplicações \textit{Web} têm sido propostos. Tecnologias para codificação de \textit{WebApps} também se desenvolveram bastante. O uso de \textit{frameworks} ou arquiteturas fundamentadas em \textit{containers} para fornecer uma sólida base para o desenvolvimento e implantação de aplicações para a \textit{Web} é quase que obrigatório atualmente. Essa infraestrutura geralmente inclui uma arquitetura MVC (\textit{Model-View-Controller})~\cite{gamma1994patterns}, interceptadores AOP (\textit{Aspect Oriented Programming}, Programação Orientada a Aspectos)~\cite{resende2005java}, um mecanismo de injeção de dependências~\cite{fowler-container}, mapeamento objeto/relacional automático para persistência de dados~\cite{bauer2005} e outras facilidades. O uso desses \textit{frameworks} reduz consideravelmente o tempo de desenvolvimento de um projeto por reutilizar código já desenvolvido, testado e documentado.

Recentemente, foi proposto o método FrameWeb (\textit{Framework-based Design Method for Web Engineering})~\cite{souza-frameweb07}, em que é sugerida a utilização de uma série de \textit{frameworks} para agilizar o desenvolvimento de uma \textit{WebApp}, juntamente com várias recomendações para aumentar a agilidade nas principais fases de desenvolvimento (requisitos, análise, projeto e outras).

O método oferece um perfil UML (extensão da linguagem UML) com quatro tipos de modelos para a fase de projeto. Tais modelos representam conceitos utilizados por algumas categorias de \textit{frameworks}, facilitando, dessa forma, a comunicação entre os projetistas e programadores durante o desenvolvimento da aplicação. Na proposta original de FrameWeb, no entanto, não foram feitos experimentos com diferentes instâncias de \textit{frameworks}. 

Para analisar a eficácia do método FrameWeb, \citeonline{duarte2014frameweb} desenvolveu uma \textit{WebApp} — um Sistema de Controle de Afastamentos de Professores, ou SCAP, para auxiliar um departamento de universidade a controlar solicitações de afastamento de seus professores efetivos. A partir dos requisitos levantados por \citeonline{duarte2014frameweb} e revisados por \citeonline{prado2015frameweb}, o objetivo deste trabalho é desenvolver o SCAP em duas novas versões, utilizando diferentes \textit{frameworks} \textit{Web}, com o intuito de analisar a eficácia do método FrameWeb em diferentes contextos, sugerindo melhorias nos modelos, quando necessário.

%%% Início de seção. %%%
\section{Objetivos}
\label{sec-intro-objetivos}

O objetivo deste trabalho é a aplicação do método FrameWeb~\cite{souza-frameweb07} em novas implementações do SCAP, utilizando os \textit{frameworks} \textit{Web} Tapestry e Wicket, a partir dos requisitos levantados por \citeonline{duarte2014frameweb} e revisados por \citeonline{prado2015frameweb}, avaliando a aplicabilidade e eficácia do método em diferentes contextos. Podemos subdividir este objetivo nos seguintes sub-objetivos:

\begin{itemize}
	
	\item O desenvolvimento do SCAP a partir de seus requisitos utilizando o \textit{frameworks} Tapestry e Wicket;
	
	\item A documentação do projeto arquitetural referente às implementações acima, utilizando FrameWeb;
	
	\item Análise da aplicabilidade de FrameWeb neste contexto e sugestões de melhoria, quando cabível, baseadas na experiência adquirida com o uso desses \textit{frameworks}.
	
\end{itemize}

\section{Metodologia}
\label{sec-intro-metodologia}

O trabalho vai ser dividido nas seguintes atividades:

\begin{enumerate}
	
	\item Pesquisa: estudo do método FrameWeb e dos diversos \textit{frameworks} nos quais ele foi inicialmente testado, bem como dos \textit{frameworks} propostos em projetos exemplos;
	
	\item Análise do Problema Proposto: análise dos requisitos do SCAP~\cite{duarte2014frameweb};
	
	\item Projeto: a partir dos resultados obtidos no item acima, construção do projeto arquitetural de duas novas versões do SCAP, utilizando FrameWeb~\cite{souza-frameweb07};
	
	\item Codificação: seguindo a arquitetura projetada, implementação do SCAP utilizando os \textit{frameworks} propostos;
	
	\item Apresentação do Projeto: apresentação final, na qual o projeto e a ferramenta SCAP com diferentes implementações serão entregues e demonstradas.
	
\end{enumerate}

\section{Organização da Monografia}
\label{sec-intro-organizacao}

Essa monografia é construída em 5 capítulos, incluindo a introdução:

\begin{itemize}
	
	\item \textbf{Capítulo~\ref{sec-referencial} --} Fundamentação Teórica: realiza-se um levantamento dos principais temas abordados ao longo deste trabalho que são: Engenharia Web, \textit{Frameworks} e o método FrameWeb;
	
	\item \textbf{Capítulo~\ref{sec-scap} --} SCAP: realiza-se uma apresentação do sistema SCAP, ou seja, quais seus objetivos e funcionalidades;
	
	\item \textbf{Capítulo~\ref{sec-projeto} --} Projeto e Implementação: apresenta o resultado da aplicação do FrameWeb no projeto, podendo ser vistos os modelos propostos e a implementação dos mesmos com os \textit{frameworks} utilizados;
	
	\item \textbf{Capítulo~\ref{sec-conclusoes} --} Considerações Finais: apresenta as conclusões desse trabalho, analisa o desenvolvimento do SCAP em diferentes \textit{frameworks} e as expectativas para trabalhos futuros.
		
\end{itemize}