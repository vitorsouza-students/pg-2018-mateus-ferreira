% ==============================================================================
% TCC - Mateus Tassinari Ferreira
% Capítulo 2 - Referencial Teórico
% ==============================================================================
\chapter{Fundamentação Teórica}
\label{sec-referencial}

Este capítulo apresenta os principais conceitos teóricos que fundamentaram o desenvolvimento do sistema SCAP e está organizado em três seções. A Seção~\ref{sec-engweb} aborda a Engenharia Web, destacando os principais conceitos e processos utilizados. A Seção~\ref{sec-frameweb} apresenta o método FrameWeb. A Seção~\ref{sec-frameworks} fala sobre os tipos de \textit{frameworks} existentes, classificando os que serão utilizados no trabalho.

\section{Engenharia Web}
\label{sec-engweb}

No início do surgimento da \textit{Web} até hoje, ela passou de um conjunto de páginas estáticas desenvolvidas de maneira \textit{ad hoc} para ser uma das principais plataformas de comunicação.

O que vemos atualmente é que a \textit{Internet} é parte presente em nossas vidas, usada, por exemplo, como entretenimento e trabalho. Para que todo esse grande número de sistemas fosse transferido para a \textit{Web}, foi imprescindível a mudança de um conjunto de páginas estáticas para verdadeiros sistemas complexos desenvolvidos especialmente para esta plataforma.

Tais aplicações executam em um servidor conectado à \textit{World Wide Web} e podem ser acessadas de qualquer computador ou dispositivo conectado à \textit{Internet} através de um \textit{browser} (navegador da \textit{Internet}), que se utiliza de protocolos como o HTTP e da linguagem HTML para exibir em páginas \textit{Web} uma interface gráfica para o usuário. Esse desenho vem removendo os obstáculos que existiam, como o usuário que só podia acessar um sistema ou aplicação caso essa estivesse instalada em seu computador ou caso ele estivesse conectado fisicamente à rede onde a aplicação está instalada.

Tornou-se capital a aplicação de conceitos existentes na Engenharia de Software, adaptados para essa nova plataforma, no desenvolvimento das aplicações \textit{Web}. Recentemente então, observamos acontecer uma nova “Crise de Software”~\cite{gibbis1994}, porém dessa vez ocorrida no desenvolvimento de aplicações para \textit{Web} e por esse motivo chamada de “Crise Web”~\cite{murugesan1999web}. Esse fato e as características do ambiente \textit{Web}, como concorrência, carga imprevisível, disponibilidade, sensibilidade ao conteúdo, evolução dinâmica, imediatismo, segurança e estética motivou a criação da Engenharia Web (\textit{Web Engineering}), que é a Engenharia de Software voltada para o desenvolvimento de aplicações \textit{Web}~\cite{pressman2011engenharia}. 

A Engenharia Web trata de abordagens disciplinadas e sistemáticas para o desenvolvimento, a implantação e a manutenção de sistemas e aplicações baseados na \textit{Web}~\cite{murugesan1999web}. Sua essência é gerenciar a diversidade e a complexidade das aplicações e, assim, evitar falhas de projeto que possam causar grandes problemas~\cite{ginige2001web}.

Como os \textit{WebApps} foram ficando mais robustos e complexos, cresceu a dificuldade de desenvolvimento. Problemas que eram vistos no desenvolvimento de aplicações \textit{Desktop} passaram a aparecer na criação dos \textit{WebApps}. Dificuldades como: inexperiência e formação inadequada dos desenvolvedores, falta de modelos de processo, métodos inadequados, planejamento incorreto, prazos e custos excedidos, falta de documentação, dificuldades de implementação e manutenção, \textit{layout} inadequado, mudança contínua dos requisitos, da tecnologia e da arquitetura, falta de rigor no processo de desenvolvimento, dentre outros~\cite{peruch2007frameweb}.

Existe então um conjunto de atributos de qualidade a serem considerados neste caso~\cite{olsina2001}:

\begin{itemize}
	
	\item \textbf{Usabilidade:} trata-se de um requisito de qualidade como também um objetivo de aplicações \textit{Web}: permitir a acessibilidade do sistema. Logo, o site deve ter uma inteligibilidade global, permitir o \textit{feedback} e ajuda \textit{online}, planejamento da interface/aspectos estéticos e aspectos especiais (acessibilidade por deficientes);
	\item \textbf{Funcionalidade:} o software \textit{Web} deve ter capacidade de busca e recuperação de informações/\textit{links}/funções, aspectos navegacionais e relacionados ao domínio da aplicação;
	\item \textbf{Eficiência:} o tempo de resposta deve ser inferior a 10s (velocidade na geração de páginas/gráficos);
	\item \textbf{Confiabilidade:} relativa à correção no processamento de \textit{links}, recuperação de erros, validação e recuperação de entradas do usuário;
	\item \textbf{Manutenibilidade:} existe uma rápida evolução tecnológica aliada à necessidade de atualização constante do conteúdo e das informações disponibilizadas na \textit{Web}, logo o software \textit{Web} deve ser fácil de corrigir, adaptar e estender.
	
\end{itemize}

A primeira fase da Engenharia Web é a Análise de Requisitos. Requisitos de um sistema são descrições dos serviços que devem ser fornecidos por esse sistema e as suas restrições operacionais~\cite{sommerville2007engenharia}.

A análise de requisitos contém as atividades responsáveis pela esquematização do problema que a \textit{Webapp} deve solucionar e coletar os requisitos com os \textit{stakeholders} e os principais objetivos dessa fase são: definir as diversas categorias de usuário, definir os objetivos da aplicação \textit{Web} e estabelecer um conhecimento básico respondendo as questões:

\begin{itemize}
	
	\item Quais são os objetivos que a \textit{Webapp} deve atingir?
	\item Quem vai utilizar a \textit{Webapp}?
	\item Qual a principal necessidade de negócio que a \textit{Webapp} deve suprir?
	
\end{itemize}

A próxima fase sugere a criação de um modelo de análise. O modelo de análise pode ser divido em quarto tópicos: a análise de conteúdo que identifica as classes de conteúdo que devem ser fornecidas para a \textit{Webapp}, a análise de interação que descreve a forma pelo qual o usuário interage com a \textit{Webapp}, a análise funcional define as operações que são aplicadas ao conteúdo da \textit{Webapp} e a sequência de processamentos que ocorrem como consequência e por último, a análise de configuração que responsável por descrever o ambiente operacional e a infraestrutura na qual a \textit{Webapp} reside~\cite{pressman2011engenharia}.

Após o modelo de Análise da \textit{Webapp}, a Engenharia Web propõe que seja elaborado o modelo de projeto que foca em seis tópicos principais: projeto de conteúdo, projeto arquitetural, projeto de estética, projeto de navegação, projeto de interface e projeto de componentes~\cite{pressman2011engenharia}.

Na sequência do processo da Engenharia Web, encontra-se a fase de implementação, na qual o projeto é devidamente construído utilizando uma linguagem de programação escolhida de acordo com as características e requisitos do projeto. Por último, temos a fase de testes que é responsável por garantir a qualidade e garantir que a \textit{WebApp} foi construída de acordo com os requisitos obtidos na fase de levantamento de requisitos. 

Dentro desse contexto da Engenharia Web, foi proposto o método FrameWeb, um método que propõe a otimização dos processos da Engenharia Web.

\section{O método FrameWeb}
\label{sec-frameweb}

FrameWeb~\cite{souza-frameweb07} é um método, baseado em \textit{frameworks}, para o desenvolvimento de sistemas de informação \textit{Web} (\textit{Web Information Systems} – WISs). Mesmo com o uso em larga escala dos \textit{frameworks} no desenvolvimento dos aplicativos, não havia nada que abordasse diretamente aspectos característicos dos \textit{frameworks} na Engenharia Web. O método assume que determinados tipos de \textit{frameworks} serão utilizados durante a construção da aplicação, define uma arquitetura básica para o WIS e propõe modelos de projeto que se aproximam da implementação do sistema usando esses \textit{frameworks}.

O FrameWeb concentra-se na fase de projeto. No entanto, espera-se que um processo de desenvolvimento completo seja conduzido de modo a produzir um produto final de qualidade. Na fase de projeto, FrameWeb propõe que a arquitetura lógica do sistema siga o padrão \textit{Service Layer}~\cite{fowler-patterns}, representado na Figura~\ref{fig-arq-frame}. O sistema é dividido em três camadas: lógica de apresentação, lógica de negócio e lógica de acesso a dados. Essas que serão subdividas nos pacotes: Visão, Controle, Aplicação, Domínio e Persistência.

\begin{figure}
	\centering
	\includegraphics[width=.8\textwidth]{figuras/fig-arq-frame.png}
	\caption{Arquitetura padrão para WIS prescrita por FrameWeb~\cite{souza-frameweb07}.}
	\label{fig-arq-frame}
\end{figure} 

Para representar componentes típicos da plataforma \textit{Web} e dos \textit{frameworks} utilizados, o FrameWeb estende o meta-modelo da UML, especificando, assim, uma sintaxe própria. Com isso, é possível utilizá-lo para a construção de diagramas de quatro tipos~\cite{mai2017frameweb}: 

\begin{itemize}
	
	\item \textbf{Modelo de Entidades:} é um diagrama de classes da UML que representa os objetos de domínio do problema e seu mapeamento para a persistência em banco de dados relacional; 
	
	\item \textbf{Modelo de Persistência:} é um diagrama de classes da UML que representa as classes DAO existentes, responsáveis pela persistência das instâncias das classes de domínio. 
	
	\item \textbf{Modelo de Navegação:} é um diagrama de classe da UML que representa os diferentes componentes que formam a camada de Lógica de Apresentação, como páginas \textit{Web}, formulários HTML e \textit{controllers}.
	
	\item \textbf{Modelo de Aplicação:} é um diagrama de classes da UML que representa as classes de serviço, que são responsáveis pela codificação dos casos de uso, e suas dependências.
	
\end{itemize}

Atualmente, \cite{beatriz-frameweb16} propôs um meta-modelo para definir formalmente a linguagem de modelagem do FrameWeb, o que permite validar os modelos e construir outras ferramentas baseadas nesse meta-modelo, além de permitir que o método seja estendido para outros \textit{frameworks} além dos originalmente propostos por \cite{souza-frameweb07}. 

\section{Frameworks}
\label{sec-frameworks}

Os WIS, e a maioria dos \textit{WebApps}, possuem uma infraestrutura semelhante. Por isso a partir do momento em que os primeiros projetos na área foram implementados, \textit{frameworks} foram criados que generalizavam essa base e poderiam ser aproveitados para o desenvolvimento de novas aplicações.

Um \textit{framework} é uma aplicação reutilizável e semicompleta que pode ser especializada para produzir aplicações personalizadas~\cite{husted2004}. Assim, esses \textit{frameworks} permitem que sistemas \textit{Web} de grande porte sejam construídos com arquiteturas de múltiplas camadas sem muito esforço de codificação. 

Além de permitir a reutilização, um \textit{framework} minimiza o esforço de desenvolvimento de aplicações, por portar a definição da arquitetura das aplicações geradas a partir dele, como também por ter predefinido o fluxo de controle dessas aplicações~\cite{taligent1993}.

~\citeonline{souza-frameweb07}, organiza a maioria dos \textit{frameworks} de \textit{WebApps} em seis categorias diferentes:

\begin{itemize}
	
	\item \textit{Frameworks} MVC (Controladores Frontais);
	\item \textit{Frameworks} Decoradores;
	\item \textit{Frameworks} de Mapeamento Objeto/Relacional;
	\item \textit{Frameworks} de Injeção de Dependência (Inversão de Controle); 
	\item \textit{Frameworks} para Programação Orientada a Aspectos (AOP);
	\item \textit{Frameworks} para Autenticação e Autorização.
	
\end{itemize}

Os dois \textit{frameworks} utilizados neste trabalho, a saber, Wicket e Tapestry, pertencem à categoria de \textit{frameworks} MVC.

\subsection{Frameworks MVC}
\label{sec-mvc}

\textit{Model View Controller} ou Modelo Visão Controle é um padrão de arquitetura de aplicações que visa separar a lógica da aplicação (\textit{Model}) da interface do usuário (\textit{View}) e do fluxo da aplicação (\textit{Controller}), permitindo que a mesma lógica de negócios possa ser acessada e visualizada por várias interfaces.  A Figura~\ref{fig-arq-mvc} ilustra a relação entre \textit{Model}, \textit{View}, \textit{Controller} e Usuários, onde as linhas sólidas indicam associação direta e as tracejadas associações indiretas.

\begin{figure}
	\centering
	\includegraphics[width=.7\textwidth]{figuras/fig-arq-mvc.png}
	\caption{Diagrama representando a arquitetura MVC~\cite{prado2015frameweb}.}
	\label{fig-arq-mvc}
\end{figure} 

Os elementos da \textit{View} (Visão) fazem uma representação das informações do \textit{Model} (Modelo) para os usuários, a partir das quais os mesmos podem interagir com a aplicação. Essa interação é tratada pelo \textit{Controller} (Controle) que pode alterar o status dos elementos do \textit{Model}. Este que pode notificar a \textit{View} tais alterações, fazendo com que esta última possa atualizar a informação apresentada ao usuário~\cite{prado2015frameweb}.

Porém, na plataforma \textit{Web} a arquitetura MVC precisa ser levemente adaptada, visto que o \textit{Model}, situado no servidor \textit{Web}, não pode notificar a \textit{View} sobre alterações, já que esta encontra-se no navegador do lado do cliente e a comunicação é sempre iniciada pelo cliente. Portanto o nome correto para esse padrão arquitetônico, quando aplicado à \textit{Web}, seria “Controlador Frontal” (Front Controller)~\cite{alur2003,souza-frameweb07}. A Figura~\ref{fig-arq-controlador} representa o funcionamento de um Controlador Frontal na \textit{Web}.

\begin{figure}
	\centering
	\includegraphics[width=.75\textwidth]{figuras/fig-arq-controlador.png}
	\caption{Representação de um Controlador Frontal na \textit{Web}~\cite{souza-frameweb07}.}
	\label{fig-arq-controlador}
\end{figure} 


Vemos que o navegador apresenta as páginas para o cliente que faz uma requisição ao servidor, este que a delega para o Controlador Frontal que, ao interagir com as classes da aplicação, executa a ação para, em seguida, delegar o resultado para a tecnologia de visão. Os \textit{frameworks} MVC fornecem um controlador frontal a ser configurado pelo desenvolvedor para que se adapte ao seu projeto.


\subsection{Wicket}
\label{sec-wicket}

O Apache Wicket\footnote{\url{https://wicket.apache.org/}} é um \textit{framework} orientado a componentes para o desenvolvimento de aplicações \textit{web} com Java. Essa característica o torna diferente da maioria dos outros \textit{frameworks} como, por exemplo, Struts ou Spring MVC, que baseiam-se na interceptação de requisições HTTP e a execução de ações associadas a essa requisição. No Wicket, uma ação é iniciada através de um evento enviado para determinado componente. Existem outros \textit{frameworks} web orientados a componentes, por exemplo, como o próprio Tapestry e JSF. Ou seja, o processamento se inicia pela Visão que, quando necessário, obtém os dados por meio do \textit{Managed Bean} (controlador), invoca as classes necessárias do Modelo e, por fim, disponibiliza os dados necessários para a Visão poder continuar o processamento.

Podemos também destacar as seguintes características: a configuração do Wicket não exige a criação/manipulação de XMLs e as páginas \textit{web} e componentes do Wicket são objetos Java, que suportam encapsulamento, herança e eventos, isto é, o Wicket não mistura HTML com código Java nem faz uso de \textit{taglibs} (bibliotecas de \textit{tags} utilizadas por frameworks como JSF, por exemplo, para montagem das páginas HTML). Os arquivos HTML possuem apenas código HTML e são associados com classes Java por meio de atributos chamados \emph{Wicket ids}, existentes nos elementos HTML.  

\subsection{Tapestry}
\label{sec-tapestry}

O Tapestry,\footnote{\url{http://tapestry.apache.org/}} como dito acima, também é um \textit{framework} orientado a componentes para o desenvolvimento de aplicações \textit{web} com Java. 

É POJO\footnote{\textit{Plain Old Java Objects}.} puro, ou seja, você não precisa implementar interfaces. Classes Java simples (e portanto fáceis de serem testadas) fazem todo o trabalho. O framework possui ainda uma característica interessante e diferente do Wicket, que é a existência de um \textit{container} de IoC (\textit{Inversion of Control}) embutido que cuida de toda a injeção de dependências de serviços e o ciclo de vida destes objetos. 

