% ==============================================================================
% TCC - Mateus Tassinari Ferreira
% Capítulo 3 - SCAP
% ==============================================================================
\chapter{SCAP}
\label{sec-scap}

Neste capítulo apresentam-se o os requisitos do SCAP levantados anteriormente por~\citeonline{duarte2014frameweb} e ~\citeonline{prado2015frameweb}. A Seção~\ref{sec-escopo} descreve o escopo do sistema; a Seção~\ref{sec-casos} apresenta os requisitos levantados na forma de casos de uso; e a Seção~\ref{sec-analise} mostra o diagrama de classes pela análise de tais requisitos.

Os requisitos apresentados aqui foram usados como base para construção de novos modelos FrameWeb e implementação de novas versões do SCAP, considerando os \textit{frameworks} Wicket e Tapestry, a serem apresentados no capítulo seguinte.

\section{Descrição do Escopo}
\label{sec-escopo}

O SCAP é um sistema para controle de afastamento de professores do Departamento de Informática (DI) da UFES, onde devem estar disponíveis informações sobre as solicitações de afastamento. As solicitações de afastamento podem ocorrer tanto para eventos no Brasil quanto no exterior. Essas solicitações precisam ser avaliadas pelos professores do DI e, em alguns casos, pela Câmara Departamental do Centro Tecnológico (CT) e pela Pró-reitoria de Pesquisa e Pós-Graduação (PRPPG) para que sejam aprovadas e o professor possa realizar sua viagem. 

Caso o pedido de afastamento realizado para o professor seja para um evento que será realizado no Brasil o professor precisa da aprovação de seu chefe direto, que é o professor chefe do Departamento ao qual pertence, e também nenhum funcionário e representante discente do DI se manifeste contra em 10 dias. Dessa forma, para realizar um afastamento para um evento nacional a avaliação da solicitação não sai de dentro do DI. 

No caso de um pedido de afastamento para um evento que acontecerá no exterior, é escolhido um professor que não tenha relação de parentesco com
o solicitante para ser o relator do pedido. Após parecer do relator, o pedido passa por aprovação no departamento como no caso acima. Porém, também será necessário que o professor receba a aprovação do CT e da PRPPG para que a solicitação de afastamento seja aprovada e publicada em Diário Oficial da União. 

No entanto, o SCAP trata de informações de solicitação de afastamentos apenas dentro do Departamento de Informática (DI) da UFES, de forma que os processos utilizados pelo CT e pela PRPPG evadem ao escopo do sistema e não há nenhuma integração com o sistema de acompanhamento de Processo da UFES. Ele deverá, no entanto, lidar com os pedidos de afastamento no exterior enquanto se encontrarem dentro do departamento. 

Podemos dizer, então, que o SCAP foi projetado para que possa auxiliar o professor nessas solicitações e para tornar o processo de solicitação de afastamento mais dinâmico, através do envio de e-mails automáticos para os interessados e do uso de formulários para ajudar na criação dos documentos necessários para se realizar uma solicitação. O sistema também permite que os demais professores e funcionários do DI possam consultar as solicitações de afastamento em andamento. 

\section{Modelo de Casos de Uso}
\label{sec-casos}

\citeonline{duarte2014frameweb} descreveu e identificou os atores do sistema SCAP, de acordo com a Tabela~\ref{tbl-scap-atores}.

\begin{table}[h]
	\caption{Atores do SCAP~\cite{duarte2014frameweb}.}
	\label{tbl-scap-atores}
	\centering
	\includegraphics[width=\textwidth]{figuras/fig-tab-atores.png}		 	
\end{table}

Os secretários suportam a parte administrativa, é responsabilidade deles cadastrar os professores, bem como mandatos dos chefes do departamento e relações de parentesco. Também cadastram os pareceres de fora do DI e arquivam os pedidos quando estiverem devidamente regularizados.

Os professores cadastram seus pedidos de afastamento no sistema e ainda podem se manifestar contra o afastamento de outro professor, caso ainda seja tempo hábil para tal. Se for adicionado como relator de um afastamento no exterior, é responsabilidade do professor decidir se o DI aprova ou não tal afastamento.

O chefe do departamento é um professor que foi selecionado para exercer a função administrativa de chefe do DI por um mandato. Assim ele pode executar todos os casos de uso associados a um professor, e é de responsabilidade dele encaminhar solicitações a relatores que deferirão pareceres sobre afastamentos no exterior.

O SCAP foi dividido em dois subsistemas: o Núcleo que contém os casos de uso referente aos atores Professor e Chefe de Departamento, enquanto o subsistema Secretaria os casos de usos referentes ao ator Secretário. Nas figuras~\ref{fig-caso-nucleo} e~\ref{fig-caso-secre} são apresentados os diagramas de caso de uso destes subsistemas. Nos parágrafos seguintes apresentamos resumidamente a descrição dos casos de uso levantados.

\begin{figure}
	\centering
	\includegraphics[width=.7\textwidth]{figuras/fig-caso-nucleo.png}
	\caption{Diagrama de Casos de Uso do subsistema Núcleo~\cite{prado2015frameweb}.}
	\label{fig-caso-nucleo}
\end{figure} 

No caso de uso \textbf{Solicitar Afastamento} o professor cadastra uma solicitação de afastamento no sistema para que seja analisado, podendo somente ele ou o chefe do departamento cancelar o pedido por meio do caso de uso \textbf{Cancelar Afastamento}.

O caso de uso \textbf{Encaminhar Afastamento} é executado quando ocorre uma solicitação de afastamento internacional e é realizado pelo Chefe do Departamento, que determina um professor para ser relator deste afastamento. O caso de uso \textbf{Definir Parecer} é realizado quando o professor escolhido como relator de um afastamento internacional cadastra no sistema o parecer sobre a referida solicitação.

O caso de uso \textbf{Consultar Afastamento} é utilizado quando o professor, chefe de departamento ou secretário busca informação sobre um pedido de afastamento. Quando um professor decide se manifestar contra um afastamento nacional ele faz uma busca por esse afastamento e logo depois utiliza o caso de uso \textbf{Manifestar-se Contra Afastamento} para realizar a sua manifestação.

\begin{figure}
	\centering
	\includegraphics[width=.7\textwidth]{figuras/fig-caso-secre.png}
	\caption{Diagrama de Casos de Uso do subsistema Secretaria~\cite{prado2015frameweb}.}
	\label{fig-caso-secre}
\end{figure} 

Os secretários são responsáveis pela parte administrativa, logo utilizam-se dos casos de uso \textbf{Cadastrar Usuário} e \textbf{Cadastrar Chefe do Departamento} para adicionar um novo professor ou secretário ao sistema e registrar um professor previamente cadastrado como chefe do departamento, informando a data de início e fim do seu mandato.

Para um pedido de afastamento internacional é necessário inserir no sistema os pareceres de outras instâncias de aprovação e, para essas tarefas, utiliza-se os casos de uso \textbf{Registrar Parecer CT} e \textbf{Registrar Parecer PRPPG} para cadastro destes.

O caso de uso \textbf{Arquiva Afastamento} acontece no final da tramitação onde ocorre a mudança de status para “Arquivado”.

\section{Análise}
\label{sec-analise}

Na criação do diagrama de classes deve se destacar a relevância da multiplicidade das relações que afeta diretamente a persistência de dados. O SCAP foi divido em dois subsistemas, entretanto as classes de domínio do problema são representadas em um único diagrama, que pode ser observado na Figura~\ref{fig-diagrama-classe}. Todas as classes, portanto, pertencem ao subsistema Núcleo. Segundo \citeonline{prado2015frameweb}, tal divisão foi realizada para facilitar a implementação e não devido à natureza das classes criadas.
	
\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/fig-diagrama-classe.png}
	\caption{Diagrama de Classes do SCAP~\cite{prado2015frameweb}.}
	\label{fig-diagrama-classe}
\end{figure} 
	
As classes \textbf{Professor} e \textbf{Secretário} representam os professores e secretários do departamento e herdam os atributos da classe \textbf{Pessoa}. Os professores que possuem relação de parentesco são representadas pela classe \textbf{Parentesco}. A classe \textbf{Mandato} representa um professor que é escolhido como chefe de departamento.

A classe \textbf{Afastamento} possui os dados referentes a uma solicitação de afastamento realizada por um professor. Esta pode conter documentos que são representados pela classe \textbf{Documento}. Se o afastamento for para fora do Brasil ele precisa de um relator que está relacionado na classe \textbf{Relator}.

A classe \textbf{Parecer} representa os pareceres dos professores e do relator (para o caso de afastamento internacional), bem como os pareceres do CT e PRPPG, e cada parecer está relacionado a um afastamento.	

Restrições de Integridade complementam as informações de um modelo deste tipo e capturam restrições relativas a relacionamentos entre elementos de um modelo que normalmente não são passíveis de serem capturadas pelas notações gráficas utilizadas na elaboração de modelos conceituais estruturais. Tais regras devem ser documentadas junto ao modelo conceitual estrutural do sistema~\cite{falboProjeto}.

Listamos abaixo as restrições de integridade que foram obtidas por \citeonline{duarte2014frameweb} e também as acrescentadas por \citeonline{prado2015frameweb}, juntamente com novas restrições referentes a funcionalidades que não foram contempladas em projetos passados do SCAP.

\begin{itemize}
	\item Um professor não pode ser solicitado para dar um parecer sobre sua própria solicitação de afastamento;
	\item A data de início de um afastamento não pode ser posterior à data de fim do mesmo afastamento;
	\item A data de início de um mandato de professor não pode ser posterior à data de fim do mesmo mandato;
	\item Não pode haver mais de dois professores (chefe e subchefe de departamento) exercendo um mandato ao mesmo tempo;
	\item O secretário do departamento não pode abrir uma solicitação de afastamento;
	\item Um professor não pode ser relator de um afastamento solicitado por um parente.	
\end{itemize}
